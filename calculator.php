<!DOCTYPE html>
<html lang = "en">

<head>
	<meta charset="utf-8"/>
	<title>Calculator</title>
</head>

<body>

	<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="GET">
		Number 1:	
		<input type="number" step="any" id="number1" name="num1"/ value="
			<?php if( isSet($_GET['num1']) ) {
				printf("%s", $_GET['num1']);
				} 
				else{
					echo("0");
				} ?>"/><br>
		Number 2:
		<input type="number" step="any" id="number2" name="num2" value="
			<?php if( isSet($_GET['num2']) ) {
				printf("%s", $_GET['num2']);
				} 
				else{
					echo("0");
				} ?>"/><br>
		<input type="radio" name="operator" value="multiply"> x <br>
		<input type="radio" name="operator" value="divide"> / <br>
		<input type="radio" name="operator" value="add" checked> + <br>
		<input type="radio" name="operator" value="subtract"> - <br>
		
		<input type="submit" value="Calculate">
		
	</form>	

	<?php
		if ( isset($_GET['operator']) And isset($_GET['num1']) And isset($_GET['num2']) ) {
			$num1 = $_GET['num1'];
			$num2 = $_GET['num2'];
			$operator = $_GET['operator'];
			
			switch ($operator) {			
				case "multiply":
					$answer = $num1*$num2;
					printf("<div id='answer'>%s x %s = %s </div>", $num1, $num2, $answer);
					break;
				
				case "divide":			
					if ($num2 != 0) {
					$answer = $num1/$num2;
						printf("<div id='answer'>%s / %s = %s </div>", $num1, $num2, $answer);
					}
					else {
						printf("<div id='answer'>Null (divided by zero) </div>");
					}
					break;
				case "add":
					$answer = $num1+$num2;
					printf("<div id='answer'>%s + %s = %s </div>", $num1, $num2, $answer);
					break;
				
				case "subract":
					$answer = $num1-$num2;
					printf("<div id='answer'>%s - %s = %s </div>", $num1, $num2, $answer);
					break;
				
				default:
					printf("<div id='answer'>Error!</div>");
					break;
			}
			
		}
	?>

</body>

</html>